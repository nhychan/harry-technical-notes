# Mounting drives

In order to access files from another computer on the network, or to access files from an external media, it is a good idea to mount the folder / filesystem as a drive on the client. 

This tutorial contains sections for the following purposes:

1. Mounting External Hard Disk on Linux operating systems.
2. Mounting NFS share from Linux host on Linux client.
3. Mounting SMB/CIFS share from Windows host on Linux client.
4. Mounting NFS share from Linux host on Windows client.

## Linux mount options

### `mount` command

Mount options are specified by `-o` in the `mount` command. Here are some common mount options:

- `uid`: User ID assigned to the mount point
- `gid`: Group ID assigned to the mount point
- `credentials`: (For **CIFS** mount or for any mounts that require login for access) The path for the credential file. Instead of this, you could use `username`, `password` and `domain` etc. to specify the credentials, but this is **not recommended** because it displays your credentials in pure text and all users on the system could access.
- `vers`: (For CIFS mount) The CIFS version on the host (server) machine. Check the related section for more information.
- `file_mode`, `dir_mode`: Rights for files and directories in the mount point, specified by a four-digit octal value.
- `iocharset`: Usually set as `utf8` as a habit

An example for using options in the mount command:

    sudo mount -o uid=1000,gid=1000,file_mode=0755,dir_mode=0755 /dev/sda1 /mnt/hello/world

## External Hard Disk

### Obtaining the UUID or the device file location

The UUID (Universially Unique IDentifier) or the device file could be obtained by using the `blkid` command. This command has to be run under administrative privileges.

In fact, the UUID approach is recommended since the order of connection of USB drives to the computer can affect the device file created.

### Temporary mount (`mount` command)

1. If you have the UUID obtained for the drive (recommended):
        
        sudo mount -U <UUID> <mount_point>

2. If you have the device file for the drive:

        sudo mount <device_file> <mount_point>

For the mount command, you can specify options for access rights and ownership as mentioned previously.

### On boot (`fstab`)

1. If you have the UUID obtained for the drive (recommended):

        UUID=<uuid> /media/external ntfs rw,nosuid,nodev,default_permissions 0 0

2. If you have the device file for the drive:
        
        <device_file> /media/external ntfs rw,nosuid,nodev,default_permissions 0 0

## Accessing NFS share on Linux host from Linux client

This section is for mounting a Linux share on another Linux client. Here, we assume that we would like to make the share on `192.168.1.100` (Host) available on `192.168.1.200` (Client). The host has a target directory `192.168.1.100:/home/user/share` that would be mapped to the mount point `192.168.1.200:/mnt/network/share` on the client.

### Temporary mount (`mount` command)

On the client (`192.168.1.200`), run the command

    sudo mount 192.168.1.100:/home/user/share /mnt/network/share

### On boot (`fstab`)



## Accessing Shared Folder on Windows host from Linux client

Before mounting the share on the Linux, the target directory / disk (target) should be configured to allow sharing.

1. Open **Properties** for that target.
2. Navigate to the **Sharing** tab. 
3. Select **Advanced Sharing**.
4. Check **Share this folder**.
5. Configure the **limit for number of concurrent connections** as per necessary. It is recommended to set it to minimal per need.
6. Configure **Permissions**.
7. To maximize safety, uncheck all rights for *Everyone*.
8. Click **Add**.
9. Enter the target username which you are going to use to access the directory. If necessary, use **Advanced...** to search for the correct user. 
    
    (For **User directories**, only the **owner** can access whatever setting you apply here. If you are not logging in as the owner, *Permission denied* would be thrown when you try to access the folder from the mount)
10. Check **Full Control** for the user.
11. Configure the **Share name** to make it easier to reference when mounting.

In this section, assume that the host is `192.168.1.100` and the client is `192.168.1.200`.

### Prerequisites for Linux Client

`cifs-utils` shld be installed.

### Credentials file

Create a credential file as following: (assume `/home/user/.smb`)
        
        username=<username>
        password=<password>
        domain=<workgroup of the user>

Note: Usually this is the most difficult steps among all steps for setting up CIFS share (Windows map to Linux), particularly the `domain` value.

For local Windows accounts, the username and password are quite straightforward. For domain, the value should be the workgroup of the computer, usually `WORKGROUP`.

For Microsoft Accounts, which is the major type of accounts used for logging into Windows 10, the username should be the email address used for logging into the Microsoft Account, and the password should be that for the Microsoft Account (if it does not work, you might also try the local login password). For the domain, use `MicrosoftAccount`.

If the above setting does not work, it might be useful to try disabling the PIN logon for Windows 10.

Remember to set this file to **700** right to avoid unauthorized access to the credential information.

### Temporary mount (`mount` command)

`mount -t cifs //192.168.1.100/<share_name> /mnt/network/mount -o credentials=/home/user/.smb,vers=3.0,uid=<userid>,gid=<groupid>,file_mode=0755,dir_mode=0755,iocharset=utf8`

### On boot (`fstab`)

1. Set up the mount point (e.g. `/mnt/network/mount`)

        mkdir -p /mnt/network/mount
        chmod 777 /mnt/network/mount

2. Update `/etc/fstab`:

        //192.168.1.100/<share_name>        /mnt/network/mount1   cifs    credentials=/home/user/.smb,vers=3.0,uid=<userid>,gid=<groupid>,file_mode=0755,dir_mode=0755,iocharset=utf8        0       0

    Note: To access a Windows share from a Linux on VirtualBox VM, the host domain address (`192.168.1.100`) should be changed to `192.168.56.1`. (This could be found from `ipconfig` on command prompt in the item `Ethernet adapter VirtualBox Host-Only Network`).

    The CIFS version (`vers` option) should be configured with reference to the Windows operating system.

## Accessing Linux Share from Windows client

To mount Linux share on Windows, the sharing would be done using `samba` instead.

### Linux prerequisite

`samba` should be installed on the Linux server hosting the shared folder.

For VirtualBox VMs, you need to enable an additional **Host-only adapter**. The IP address of the VM could be found by `ifconfig` command, usually `192.168.56.101` or similar. This would be used for accessing the share.

### Configuring `samba`

1. Set up a password for the current user to access the share using the command

        smbpasswd

    Alternatively, use

        smbpasswd -a -U <user>

    to specify a new user.

2. Edit the config file `/etc/samba/smb.conf`: (The config below are for reference only)

    Read-only share:

        [<name-of-share>]
        comment = <comment>
        path = <path-to-directory>
        available = yes
        valid users = <usernames>
        read only = yes
        browsable = yes
        public = yes
        writable = no

    Read/write share

        [<name-of-share>]
        comment = <comment>
        path = <path-to-directory>
        available = yes
        valid users = <usernames>
        read only = no
        browsable = yes
        public = yes
        writable = yes

3. Use `testparm` to check if the configuration works.

4. Access the share using the address `\\192.168.56.101\<name-of-share>`.

References: 
1. http://timlehr.com/auto-mount-samba-cifs-shares-via-fstab-on-linux/ (For Windows mount)
2. https://ubuntuforums.org/showthread.php?t=1432413 (For Linux external HDD mount)
3. https://www.xltoolbox.net/blog/2013/11/local-file-sharing-with-samba-and-virtualbox-guests.html (for mounting Linux share on Windows)
4. https://superuser.com/questions/258026/using-samba-to-share-a-folder-from-a-linux-guest-with-a-windows-host-in-virtualb (for mounting Linux share on Windows)