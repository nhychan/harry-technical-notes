# SSH Tunnels

SSH Tunnels can be used for port forwarding and a number of different applications.

## Types of SSH Tunnels (Port Forwarding)

### 1. Local

A local SSH tunnel allows connection to the remote server port from the local client port.

    autossh -L -nNT [host_port]:localhost:[dest_port] [user]@[dest_address] -p [dest_ssh_port]

Put in simple terms, by SSH forwarding using the command above, the port `localhost:[host_port]` is used to access `[dest_address]:[target_dest_port]`.

Actual use: To proxy a connection to a restricted access port.

### 2. Remote

A remote SSH tunnel allows connection back to the local client port from remote server port.

    autossh -R -nNT [target_dest_port]:localhost:[host_port] [user]@[dest_address] -p [dest_ssh_port]

Put in simple terms, by the above command, the port `[dest_address]:[target_dest_port]` is used to access `localhost:[host_port]`,

Actual use: To allow access to a restricted server from remote, or to "expose" a port to outside access.

### 3. Dynamic

## Generating public keys for SSH

*Assume that you would like to generate keys for SSH connection to `me@somewhere.cc:422`.*

1. Run the following command to generate a private key file.

        ssh-keygen

    Assume that the private key file was stored at its default location `/home/user/.ssh/id_rsa`, and the public key file `/home/user/.ssh/id_rsa.pub`

2. Copy the key to the remote server. If you have specified another path for the **private** key, remember to specify it using the `-i <FILE>` option.

        ssh-copy-id [-i <FILE>] me@somewhere.cc -p 422

## SSH Tunnel at startup on Linux

To setup SSH port forwarding on Linux that automatically starts when the server boots, use the following steps: (Requires `sudo` privilege):

*Assume that you would like to set up SSH tunnel to `me@somewhere.cc:422` for mapping `somewhere.cc:22` to `localhost:13022` (access to remote domain by local port).*

1. Wrap the command in a bash file, for example `/home/user/cstunnel.sh`.

        autossh -L -nNT 13022:localhost:22 me@somewhere.cc -p 422 -i /home/user/.ssh/id_rsa

   Remember to make it executable.

        chmod +x /home/user/cstunnel.sh

2. Create symlink to the script in `/usr/bin`, for example `/usr/bin/mytun1`

        ln -s /home/user/cstunnel.sh /usr/bin/mytun1

3. Create a unit file in `/lib/systemd/system/` to define a `systemd` service for the tunnel.
    
        # /lib/systemd/system/mytun1.service

        [Unit]
        Wants=network-online.target
        After=network.target network-online.target
        Description=My First SSH Tunnel.

        [Service]
        Type=simple
        ExecStart=/bin/bash /usr/bin/mytun1

        [Install]
        WantedBy=multi-user.target

    Here, the `Wants=network-online.target` and `After=network.target network-online.target` would make the service to be started after network is available. 

    `ExecStart=/bin/bash /usr/bin/mytun1` would execute the script for SSH tunnel when the service is started.

    In general, this configuration is also useful for creating regular cron tasks for which network is required.

4. Link the service file `/lib/systemd/system/mytun1.service` to `/etc/systemd/system/*`

        systemctl link /lib/systemd/system/mytun1.service

    This would create the symlink.

5. Enable the service.

        systemctl enable mytun1.service

## SSH Tunnel on Windows (and for those at startup)

Per personal experience, Bitvise SSH client is the best for maintaining SSH tunnels. It is also easy to create tunnels that would start with Windows.

1. Install Bitvise SSH client.

2. Create a profile to the server that you would like to open a tunnel to.

3. For **Local Tunnels**: (Use **C2S** connection)
    
    + Listen port: The port to listen for the connection on host machine
    + Listen interface: Host machine
    + Destination port: The port to forward on the remote machine
    + Destination host: Usually fill in `localhost`

    For **Reverse Tunnels**: (Use **S2C** connection)

    + Listen port: The port on **remote server** to listen to your connection.
    + Listen interface: Usually `localhost`
    + Destination port: The port you would like to make available on your host machine
    + Destination host: Host machine

4. Click **Log in** to start the tunnel.

5. To start the SSH tunnel with Windows:
    + **Win+R** and run `shell:startup`.
    + Find the location of BvSsh.exe in your Bitvise SSH Client installation. (Usually this path should be `C:\Program Files (x86)\Bitvise SSH Client\BvSsh.exe`)
    + Create shortcut on Desktop for `BvSsh.exe`
    + Edit the shortcut: Change the target to `"C:\Program Files (x86)\Bitvise SSH Client\BvSsh.exe" -profile="<PATH_TO_PROFILE>" -loginOnStartup`
    + Check again in the profile that you have selected to **Store encrypted password in profile** .

## Troubleshooting

### Port used by process

1. Windows (PowerShell)

        Get-Process -Id (Get-NetTCPConnection -LocalPort <portNumber>).OwningProcess

2. Linux

        netstat -tupln | grep '<portNumber>'

### SSH Connection: Connection refused

#### Scenario

A remote tunnel was constructed between remote server `234.123.56.78` port `22` to `192.168.1.123` on port `10234` to allow SSH to `234.123.56.78` through `192.168.1.123:234`. However, when running the command `ssh user@192.168.1.123 -p 10234`, a `Connection refused` error appeared.

#### Reason

The `sshd` config on `192.168.1.123` was not configured to allow `GatewayPorts` (by default).

#### Solution

Enable by modifying (or create if there is none) the line containing `GatewayPorts` into `GatewayPorts yes`.

## Practical Application: Set up VNC tunnel

### Schematic

[ Target Computer ] <6900> ===(`-R 40001:localhost:6900`)===> <40001> [ Intermediate server ]

[ Intermediate Server ] <40001> <===(`-L 6901:localhost:40001`)=== <6901> [ Client ]

### Steps

0. Prepare an **Intermediate server** which both computers (client and target computer) could access. This could be a VM or an actual server (e.g. Raspberry pi 4) accessible by a DNS domain name.

1. On the computer to be connected, install any VNC server (e.g. TightVNC) and configure the VNC server to listen to any port, say port 6900. Avoid using default port to prevent security issues.

2. Install Bitvise SSH Client (or any other SSH client that are capable of performing SSH tunneling) on the **Target computer** and set up an SSH connection to the intermediate server. Use public key authentication. (You will need to use `ssh-keygen` on Windows to generate the key, and then `ssh <user>@<host> 'cat >> .ssh/authorized_keys ; exit' < <PUBLIC-key-path>` to make it authorized)

3. Set up the **reverse SSH tunnel** using **S2C** connection: (CYou can change 25001 to any port for this and subsequent steps as you like as long as it doesn't clash with used ports)

    + Listen interface: `localhost`
    + Listen port: `25001`
    + Destination host: `localhost`
    + Destination port: `6900`

    Or, by using the SSH client of your choice, set up SSH tunnel that is equivalent to:

        ssh -N -f -R 25001:localhost:6900 <user>@<host>

4. Set up **local SSH tunnel** on the client:

    **Note**: This step is only necessary and **recommended** if you are using an unencrypted VNC server such as TightVNC. By passing the two arms of the VNC connection through SSH tunnel, the connection is essentially encrypted.
    
    For **Windows**: Install Bitvise SSH client and set up **local SSH tunnel** using **C2S** connection: 

    - Listening interface: `127.0.0.1`
    - Listening port: `6901`
    - Destination host: `localhost`
    - Destination port: `25001`

    Or, by using the SSH client of your choice, set up SSH tunnel that is equivalent to:

        ssh -N -f -L 6901:localhost:25001 <user>@<host>

    On **iOS** or **Android** devices you can use Termius:
    + Port from: `6901`
    + Destination: `localhost`
    + Port to: `25001`

### Actual Testing:

Comparing (A) RealVNC Cloud connections and (B) TightVNC Server + SSH Tunnel, the speed was 2495 kbit/s and 32786 kbit/s on (A) and (B) respectively. This is probably due to that the RealVNC server was geographically distant from both sides of the connection, creating unnecessary RTTs.

## References

https://unix.stackexchange.com/questions/194175/can-i-use-a-symbolic-link-as-a-service-of-systemd

https://www.linode.com/docs/quick-answers/linux/start-service-at-boot/

https://unix.stackexchange.com/questions/126009/cause-a-script-to-execute-after-networking-has-started

https://transang.me/ssh-tunnel-in-ubuntu/

https://stackoverflow.com/questions/48198/how-can-you-find-out-which-process-is-listening-on-a-port-on-windows